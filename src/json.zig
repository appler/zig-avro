const std = @import("std");
const mem = std.mem;
const testing = std.testing;

const replace = mem.replace;
const replacementSize = mem.replacementSize;
const Allocator = mem.Allocator;


/// Simple JSON stringify. Caller owns memory.
pub fn jsonStringify(allocator: Allocator, json_val: []const u8) ![]const u8 {
    // Remove newlines
    const size_trim = replacementSize(u8, json_val, "\n", "");
    const json_trim = try allocator.alloc(u8, size_trim);
    defer allocator.free(json_trim);
    _ = replace(u8, json_val, "\n", "", json_trim);

    // Escape quotes
    const size = replacementSize(u8, json_trim, "\"", "\\\"");
    const json_str = try allocator.alloc(u8, size);
    _ = replace(u8, json_trim, "\"", "\\\"", json_str);

    return json_str;
}

test "json stringify" {
    const allocator = testing.allocator;
    const json_val =
        \\{
        \\  "name": "John Doe",
        \\  "age": 42
        \\}
    ;

    const json_str = try jsonStringify(allocator, json_val);
    defer allocator.free(json_str);

    try testing.expectEqualStrings("{  \\\"name\\\": \\\"John Doe\\\",  \\\"age\\\": 42}", json_str);
}
