const std = @import("std");
const builtin = @import("builtin");

/// Gets a logger that will degrade error messages to debug for tests
pub fn get(comptime scope: @Type(.EnumLiteral)) type {
    return if (builtin.is_test)
        // Downgrade `err` to `debug` for tests.
        // Zig fails any test that does `log.err`, but we want to test those code paths.
        struct {
            pub const base = std.log.scoped(scope);
            pub const err = debug;
            pub const warn = base.warn;
            pub const info = base.info;
            pub const debug = base.debug;
        }
    else
        std.log.scoped(scope);
}
