const std = @import("std");
const testing = std.testing;

/// Decodes variable-length integers with unsigned LEB128 format
pub fn decodeVarInt(bytes: []const u8) u64 {
    var shift: u6 = 0;
    var val: u64 = 0;
    var i: usize = 0;
    while (true) : (i += 1) {
        const byte: u7 = @truncate(bytes[i]);
        val |= @as(u64, byte) << shift;
        if (bytes[i] >> 7 == 0) break;
        shift += 7;
    }
    return val;
}

/// Decodes bytes from variable-length zig-zag format
pub fn decodeVarIntZigZag(bytes: []const u8) i64 {
    const val = decodeVarInt(bytes);

    if (@mod(val, 2) == 1) {
        const x: i64 = @intCast(val >> 1);
        return -x - 1;
    }

    return @intCast(val >> 1);
}

/// Return length in number of bytes counted from first element in a LEB128.
/// Useful when having a byte-array that starts a LEB128 sequence but may
/// contain other bytes after.
pub fn varIntLen(bytes: []const u8) usize {
    var size: usize = 0;
    while (true) : (size += 1) {
        if (bytes[size] >> 7 == 0) break;
    }
    return size + 1;
}

test "decode var int zig-zag" {
    const bytes = [_]u8 { 254, 1 }; // 127
    const bytes2 = [_]u8 { 172, 2 }; // 150
    const bytes3= [_]u8 { 128, 128, 2 }; // 16384
    const bytes4 = [_]u8 { 130, 128, 2 }; // 16385
    const bytes5= [_]u8 { 156, 219, 2 }; // 22222
    const bytes6 = [_]u8 { 254, 255, 255, 255, 15 }; // 2147483647 (max)
    const bytes7 = [_]u8 { 0 }; // 0
    const bytes8 = [_]u8 { 1 }; // -1
    const bytes9 = [_]u8 { 255, 1 }; // -128
    const bytes10 = [_]u8 { 253, 255, 255, 255, 15 }; // -2147483647

    try testing.expectEqual(decodeVarIntZigZag(&bytes), 127);
    try testing.expectEqual(decodeVarIntZigZag(&bytes2), 150);
    try testing.expectEqual(decodeVarIntZigZag(&bytes3), 16384);
    try testing.expectEqual(decodeVarIntZigZag(&bytes4), 16385);
    try testing.expectEqual(decodeVarIntZigZag(&bytes5), 22222);
    try testing.expectEqual(decodeVarIntZigZag(&bytes6), 2147483647);
    try testing.expectEqual(decodeVarIntZigZag(&bytes7), 0);
    try testing.expectEqual(decodeVarIntZigZag(&bytes8), -1);
    try testing.expectEqual(decodeVarIntZigZag(&bytes9), -128);
    try testing.expectEqual(decodeVarIntZigZag(&bytes10), -2147483647);
}

test "variant int len" {
    const bytes = [_]u8 { 254, 1, }; // 127
    const bytes2 = [_]u8 { 254, 1, 6, 49 }; // 127
    const bytes3= [_]u8 { 128, 128, 2 }; // 16384
    const bytes4 = [_]u8 { 254, 255, 255, 255, 15 }; // 2147483647 (max)

    try testing.expectEqual(varIntLen(&bytes), 2);
    try testing.expectEqual(varIntLen(&bytes2), 2);
    try testing.expectEqual(varIntLen(&bytes3), 3);
    try testing.expectEqual(varIntLen(&bytes4), 5);
}
