const std = @import("std");
const io = std.io;
const json = @import("json");
const log = @import("log.zig").get(.avro);
const serialize = @import("serialize.zig");
const jsonStringify = @import("json.zig").jsonStringify;
const parseUrl = @import("url.zig").parseUrl;
const mem = std.mem;
const eql = mem.eql;
const testing = std.testing;
const http = std.http;
const fmt = std.fmt;

const allocPrint = fmt.allocPrint;
const ArrayList = std.ArrayList;
const Client = http.Client;
const Uri = std.Uri;

const a = @cImport({
    @cInclude("avro.h");
});

const Allocator = mem.Allocator;

pub const Error = error {
    AvroSchemaInitError,
    AvroSchemaConfigError,
    AvroValueSerializeError,
    AvroValueSchemaWhithoutId
} || json.ParseErrors;

pub const AvroValue = struct {
    allocator: Allocator,
    native: a.avro_value_t,
    schema: *AvroSchema,

    /// Create Avro value from a json
    pub fn fromJson(allocator: Allocator, json_str: []const u8, schema: *AvroSchema) Error!AvroValue {
        const json_val = try json.parse(json_str, allocator);
        defer json_val.deinit(allocator);

        const schema_class = a.avro_generic_class_from_schema(schema.native);
        var avro_val: a.avro_value_t = undefined;
        _ = a.avro_generic_value_new(schema_class, &avro_val);

        _ = try schemaTraverseJson(allocator, schema.native, json_val, &avro_val);

        return AvroValue {
            .allocator = allocator,
            .native = avro_val,
            .schema = schema,
        };
    }

    /// Create Avro value from a byte-array
    pub fn fromBytes(allocator: Allocator, val: []const u8, schema: *AvroSchema) Error!AvroValue {
        const schema_class = a.avro_generic_class_from_schema(schema.native);
        var avro_val: a.avro_value_t = undefined;
        _ = a.avro_generic_value_new(schema_class, &avro_val);

        _ = try schemaTraverseBytes(allocator, schema.native, val, &avro_val);

        return AvroValue {
            .allocator = allocator,
            .native = avro_val,
            .schema = schema,
        };
    }

    /// Return Avro value as json
    /// Caller owns memory.
    pub fn asJson(self: AvroValue) Error![]const u8 {
        var avro_json: [*c]u8 = null;
        _ = &avro_json;
        _ = a.avro_value_to_json(&self.native, @as(c_int, 0), &avro_json);

        return mem.span(avro_json);
    }

    /// Return Avro value as bytes
    /// Caller owns memory.
    pub fn asBytes(self: AvroValue) Error![]const u8 {
        var size: usize = undefined;
        _ = a.avro_value_sizeof(@constCast(&self.native), &size);

        var buf = try self.allocator.alloc(u8, size);

        const writer = a.avro_writer_memory(buf[0..].ptr, @intCast(size));
        const res = a.avro_value_write(writer, @constCast(&self.native));

        if (res != 0) {
            log.err("Failed to write avro value. Code: {d}", .{ res });
        }

        a.avro_writer_free(writer);

        return buf;
    }

    /// Return Avro value as bytes with added Confluent magic bytes for schema id
    /// Caller owns memory.
    pub fn asBytesConfluent(self: AvroValue) Error![]const u8 {
        const schema_id = self.schema.id;
        if (schema_id == 0) {
            log.err("Schema has no id. To get Avro value as Confluent bytes create an Avro schema with registry or id.", .{});
            return Error.AvroValueSchemaWhithoutId;
        }

        var size: usize = undefined;
        _ = a.avro_value_sizeof(@constCast(&self.native), &size);

        // Add magic byte (1) + schema id (4)
        const magic_size = 5;
        size += magic_size;
        var buf = try self.allocator.alloc(u8, size);

        buf[0] = 0; // magic byte

        buf[1] = @intCast((schema_id >> 24) & 0xFF);
        buf[2] = @intCast((schema_id >> 16) & 0xFF);
        buf[3] = @intCast((schema_id >> 8) & 0xFF);
        buf[4] = @intCast(schema_id & 0xFF);

        const writer = a.avro_writer_memory(buf[magic_size..].ptr, @intCast(size - magic_size));
        const res = a.avro_value_write(writer, @constCast(&self.native));

        if (res != 0) {
            log.err("Failed to write avro value. Code: {d}", .{ res });
        }

        a.avro_writer_free(writer);

        return buf;
    }

    pub fn deinit(self: AvroValue) void {
        a.avro_value_decref(@constCast(&self.native));
    }

    fn schemaTraverseBytes(allocator: Allocator, schema: a.avro_schema_t, bytes: []const u8, avro_val: *a.avro_value_t) !usize {
        const schema_type = schema.*.type;

        switch (schema_type) {
            a.AVRO_NULL => {
                _ = avro_val.iface.*.set_null.?(avro_val.iface, null);
            },
            a.AVRO_BOOLEAN => {
                _ = avro_val.iface.*.set_boolean.?(avro_val.iface, avro_val.self, @intCast(bytes[0]));
                return 1;
            },
            a.AVRO_INT32 => {
                const val = serialize.decodeVarIntZigZag(bytes);
                _ = avro_val.iface.*.set_int.?(avro_val.iface, avro_val.self, @intCast(val));

                return serialize.varIntLen(bytes);
            },
            a.AVRO_INT64 => {
                const val = serialize.decodeVarIntZigZag(bytes);
                _ = avro_val.iface.*.set_long.?(avro_val.iface, avro_val.self, val);

                return serialize.varIntLen(bytes);
            },
            a.AVRO_FLOAT => {
                const val = mem.readVarInt(u32, bytes[0..4], .little);
                _ = avro_val.iface.*.set_float.?(avro_val.iface, avro_val.self, @bitCast(val));

                return 4;
            },
            a.AVRO_DOUBLE => {
                const val = mem.readVarInt(u64, bytes[0..8], .little);
                _ = avro_val.iface.*.set_double.?(avro_val.iface, avro_val.self, @bitCast(val));

                return 8;
            },
            a.AVRO_BYTES => {
                const avro_bytes = AvroBytes.read(bytes);
                _ = avro_val.iface.*.set_bytes.?(avro_val.iface, avro_val.self, @as(?*anyopaque, @constCast(avro_bytes.val.ptr)), avro_bytes.val.len);

                return avro_bytes.len;
            },
            a.AVRO_STRING => {
                const avro_bytes = AvroBytes.read(bytes);
                const valZ = try allocator.dupeZ(u8, avro_bytes.val);
                defer allocator.free(valZ);

                _ = avro_val.iface.*.set_string.?(avro_val.iface, avro_val.self, @as([*]u8, @constCast(valZ.ptr)));

                return avro_bytes.len;
            },
            a.AVRO_RECORD => {
                const schema_len = a.avro_schema_record_size(schema);
                var offset: usize = 0;
                for (0..schema_len) |i| {
                    const field_schema = a.avro_schema_record_field_get_by_index(schema, @intCast(i));
                    var field: a.avro_value_t = undefined;

                    _ = avro_val.iface.*.get_by_index.?(avro_val.iface, avro_val.self, i, &field, null);
                    const len = try schemaTraverseBytes(allocator, field_schema, bytes[offset..], &field);

                    offset += len;
                }
                return offset;
            },
            a.AVRO_ENUM => {
                _ = avro_val.iface.*.set_enum.?(avro_val.iface, avro_val.self, bytes[0]);
                return 1;
            },
            a.AVRO_ARRAY => {
                const varint_len = serialize.varIntLen(bytes);
                const arr_len: usize = @intCast(serialize.decodeVarIntZigZag(bytes[0..varint_len]));

                const arr_schema = a.avro_schema_array_items(schema);
                var val: a.avro_value_t = undefined;

                var offset = varint_len;
                for (0..arr_len) |_| {
                    _ = avro_val.iface.*.append.?(avro_val.iface, avro_val.self, &val, null);
                    const val_len = try schemaTraverseBytes(allocator, arr_schema, bytes[offset..], &val);
                    offset += val_len;
                }

                return offset + 1;
            },
            a.AVRO_MAP => {
                const map_schema = a.avro_schema_map_values(schema);
                var val: a.avro_value_t = undefined;

                const map_var_len = serialize.varIntLen(bytes);
                const map_len: usize = @intCast(serialize.decodeVarIntZigZag(bytes));
                var offset = map_var_len;
                for (0..map_len) |_| {
                    const key_avro_bytes = AvroBytes.read(bytes[offset..]);
                    const keyZ = try allocator.dupeZ(u8, key_avro_bytes.val);
                    defer allocator.free(keyZ);

                    _ = avro_val.iface.*.add.?(avro_val.iface, avro_val.self, @as([*]u8, @constCast(keyZ.ptr)), &val, 0, 0);
                    offset += key_avro_bytes.len;

                    const val_len = try schemaTraverseBytes(allocator, map_schema, bytes[offset..], &val);
                    offset += val_len;
                }

                return offset + 1;
            },
            a.AVRO_UNION => {
                const union_index = serialize.varIntLen(bytes);
                const index_len: usize = @intCast(serialize.decodeVarIntZigZag(bytes));

                var branch_val: a.avro_value_t = undefined;
                _ = avro_val.iface.*.set_branch.?(avro_val.iface, avro_val.self, @intCast(union_index), &branch_val);

                const branch_schema = a.avro_schema_union_branch(schema, @intCast(union_index));
                const offset = try schemaTraverseBytes(allocator, branch_schema, bytes[index_len..], &branch_val);

                return index_len + offset;
            },
            a.AVRO_FIXED => {
                const fixed_size: usize = @intCast(a.avro_schema_fixed_size(schema));
                const val: ?*anyopaque = @constCast(bytes[0..fixed_size].ptr);
                _ = avro_val.iface.*.set_fixed.?(avro_val.iface, avro_val.self, val, fixed_size);

                return fixed_size;
            },
            else => |i| {
                log.warn("Unsupported type: {d}\n", .{ i });
            }
        }

        return 0;
    }

    /// Traverse JSON value based on Avro schema to construct an Avro value.
    /// This function is used internally by AvroValue.
    fn schemaTraverseJson(allocator: Allocator, schema: a.avro_schema_t, json_val: *json.JsonValue, avro_val: *a.avro_value_t) !u8 {
        const schema_type = schema.*.type;
        switch (schema_type) {
            a.AVRO_NULL => {
                if (json_val.value != null) {
                    return 1;
                }
                _ = avro_val.iface.*.set_null.?(avro_val.iface, null);
            },
            a.AVRO_BOOLEAN => {
                if (json_val.value == null or json_val.type != .boolean) {
                    return 1;
                }
                _ = avro_val.iface.*.set_boolean.?(avro_val.iface, avro_val.self, @as(c_int, @intFromBool(json_val.boolean())));
            },
            a.AVRO_INT32 => {
                if (json_val.value == null or json_val.type != .integer) {
                    return 1;
                }
                _ = avro_val.iface.*.set_int.?(avro_val.iface, avro_val.self, @intCast(json_val.integer()));
            },
            a.AVRO_INT64 => {
                if (json_val.value == null or json_val.type != .integer) {
                    return 1;
                }
                _ = avro_val.iface.*.set_long.?(avro_val.iface, avro_val.self, json_val.integer());
            },
            a.AVRO_FLOAT => {
                if (json_val.value == null or json_val.type != .float) {
                    return 1;
                }
                _ = avro_val.iface.*.set_float.?(avro_val.iface, avro_val.self, @floatCast(json_val.float()));
            },
            a.AVRO_DOUBLE => {
                if (json_val.value == null or json_val.type != .float) {
                    return 1;
                }
                _ = avro_val.iface.*.set_double.?(avro_val.iface, avro_val.self, json_val.float());
            },
            a.AVRO_BYTES => {
                if (json_val.value == null or json_val.type != .string) {
                    return 1;
                }
                const val: ?*anyopaque = @constCast(json_val.string().ptr);
                _ = avro_val.iface.*.set_bytes.?(avro_val.iface, avro_val.self, val, json_val.string().len);
            },
            a.AVRO_STRING => {
                if (json_val.value == null or json_val.type != .string) {
                    return 1;
                }
                const valZ = try allocator.dupeZ(u8, json_val.string());
                defer allocator.free(valZ);
                _ = avro_val.iface.*.set_string.?(avro_val.iface, avro_val.self, valZ);
            },
            a.AVRO_RECORD => {
                const schema_len = a.avro_schema_record_size(schema);

                for (0..schema_len) |i| {
                    const name = a.avro_schema_record_field_name(schema, @intCast(i));
                    const json_obj = json_val.get(std.mem.span(name));

                    const field_schema = a.avro_schema_record_field_get_by_index(schema, @as(c_int, @intCast(i)));
                    var field: a.avro_value_t = undefined;
                    _ = avro_val.iface.*.get_by_index.?(avro_val.iface, avro_val.self, i, &field, null);

                    _ = try schemaTraverseJson(allocator, field_schema, json_obj, &field);
                }
            },
            a.AVRO_ENUM => {
                if (json_val.value == null or json_val.type != .string) {
                    return 1;
                }
                const valZ = try allocator.dupeZ(u8, json_val.string());
                defer allocator.free(valZ);
                const enum_val = a.avro_schema_enum_get_by_name(schema, valZ);
                _ = avro_val.iface.*.set_enum.?(avro_val.iface, avro_val.self, enum_val);
            },
            a.AVRO_ARRAY => {
                if (json_val.value == null or json_val.type != .array) {
                    return 1;
                }

                const arr_len = json_val.array().len();
                const arr_schema = a.avro_schema_array_items(schema);
                var val: a.avro_value_t = undefined;

                for (0..arr_len) |i| {
                    _ = avro_val.iface.*.append.?(avro_val.iface, avro_val.self, &val, null);
                    _ = try schemaTraverseJson(allocator, arr_schema, json_val.array().get(i), &val);
                }
            },
            a.AVRO_MAP => {
                if (json_val.value == null or json_val.type != .object) {
                    return 1;
                }

                const map_schema = a.avro_schema_map_values(schema);
                var val: a.avro_value_t = undefined;
                const obj = json_val.object();
                var iter = obj.map.iterator();

                while (iter.next()) |e| {
                    const keyZ = try allocator.dupeZ(u8, e.key_ptr.*);
                    defer allocator.free(keyZ);

                    _ = avro_val.iface.*.add.?(avro_val.iface, avro_val.self, @as([*]u8, @constCast(keyZ.ptr)), &val, 0, 0);
                    _ = try schemaTraverseJson(allocator, map_schema, e.value_ptr.*, &val);
                }
            },
            // The JSON parser can't see the difference of union values that
            // share the same JSON type such as AVRO_STRING and AVRO_BYTES (both
            // strings in JSON) or AVRO_INT32 and AVRO_INT64 (both ints). It
            // will just pick the first match in the union that matches the JSON
            // type.
            //
            // This is a shortcoming of trying to guess the Avro type just from
            // a JSON value.
            a.AVRO_UNION => {
                var branch: a.avro_value_t = undefined;

                for (0..a.avro_schema_union_size(schema)) |i| {
                    _ = avro_val.iface.*.set_branch.?(avro_val.iface, avro_val.self, @as(c_int, @intCast(i)), &branch);
                    const avro_type = a.avro_schema_union_branch(schema, @as(c_int, @intCast(i)));
                    if (try schemaTraverseJson(allocator, avro_type, json_val, &branch) == 0)
                        break;
                }
            },
            a.AVRO_FIXED => {
                if (json_val.value == null or json_val.type != .string) {
                    return 1;
                }
                const val: ?*anyopaque = @constCast(json_val.string().ptr);
                const fixed_size = a.avro_schema_fixed_size(schema);
                _ = avro_val.iface.*.set_fixed.?(avro_val.iface, avro_val.self, val, @intCast(fixed_size));
            },
            else => |i| {
                log.warn("Unsupported type: {d}", .{ i });
            }
        }

        return 0;
    }
};

pub const AvroSchema = struct {
    allocator: Allocator,
    native: a.avro_schema_t,
    id: usize,
    json: []const u8,

    /// Create Avro schema from JSON
    /// Schema id will be set to 0
    pub fn fromJson(allocator: Allocator, schema: []const u8) Error!AvroSchema {
        var avro_schema: a.avro_schema_t = undefined;
        const res = a.avro_schema_from_json_length(@constCast(schema.ptr), schema.len, &avro_schema);
        errdefer log.err("{s}\n", .{ a.avro_strerror() });

        if (res != 0) {
            return Error.AvroSchemaInitError;
        }

        return AvroSchema {
            .allocator = allocator,
            .native = try jsonToAvro(schema),
            .id = 0,
            .json = try allocator.dupe(u8, schema)
        };
    }

    /// Create Avro schema from JSON with id
    pub fn fromJsonWithId(allocator: Allocator, schema: []const u8, id: usize) Error!AvroSchema {
        var avro_schema: a.avro_schema_t = undefined;
        const res = a.avro_schema_from_json_length(@constCast(schema.ptr), schema.len, &avro_schema);
        errdefer log.err("{s}\n", .{ a.avro_strerror() });

        if (res != 0) {
            return Error.AvroSchemaInitError;
        }

        return AvroSchema {
            .allocator = allocator,
            .native = try jsonToAvro(schema),
            .id = id,
            .json = try allocator.dupe(u8, schema)
        };
    }

    /// Create Avro schema from JSON and add to schema registry
    /// Returnd schema will have id from registry
    pub fn fromJsonWithRegistry(allocator: Allocator, schema: []const u8, schema_registry: []const u8) !AvroSchema {
        var avro_schema: a.avro_schema_t = undefined;
        const res = a.avro_schema_from_json_length(@constCast(schema.ptr), schema.len, &avro_schema);
        errdefer log.err("{s}\n", .{ a.avro_strerror() });

        if (res != 0) {
            return Error.AvroSchemaInitError;
        }

        const id = try addToRegistry(allocator, schema, schema_registry);

        return AvroSchema {
            .allocator = allocator,
            .native = try jsonToAvro(schema),
            .id = id,
            .json = try allocator.dupe(u8, schema)
        };
    }

    /// Create avro schema by id from registry
    pub fn fromRegistryById(allocator: Allocator, id: usize, schema_registry: []const u8) !AvroSchema {
        const path = try allocPrint(allocator, "/schemas/ids/{d}", .{ id });
        defer allocator.free(path);

        const res_body = try fetchBody(allocator, schema_registry, path);
        defer allocator.free(res_body);

        const json_val = try json.parse(res_body, allocator);
        defer json_val.deinit(allocator);

        const schema = json_val.get("schema").string();
        return AvroSchema {
            .allocator = allocator,
            .native = try jsonToAvro(schema),
            .id = id,
            .json = try allocator.dupe(u8, schema)
        };
    }

    /// Create Avro schema by name with latest version from registry
    pub fn fromRegistryByName(allocator: Allocator, name: []const u8, schema_registry: []const u8) !AvroSchema {
        const path = try allocPrint(allocator, "/subjects/{s}/versions/latest", .{ name });
        defer allocator.free(path);

        const res_body = try fetchBody(allocator, schema_registry, path);
        defer allocator.free(res_body);

        const json_val = try json.parse(res_body, allocator);
        defer json_val.deinit(allocator);

        const schema = json_val.get("schema").string();
        return AvroSchema {
            .allocator = allocator,
            .native = try jsonToAvro(schema),
            .id = @intCast(json_val.get("id").integer()),
            .json = try allocator.dupe(u8, schema)
        };
    }

    pub fn deinit(self: AvroSchema) void {
        _ = a.avro_schema_decref(self.native);
        self.allocator.free(self.json);
    }

    /// Add Avro schema to registry by URL
    /// Returns id of created schema
    pub fn addToRegistry(allocator: Allocator, schema: []const u8, schema_registry: []const u8) !usize {
        const json_val = try json.parse(schema, allocator);
        defer json_val.deinit(allocator);

        const path = try allocPrint(allocator, "/subjects/{s}/versions", .{ json_val.get("name").string() });
        defer allocator.free(path);
        const url = try parseUrl(allocator, schema_registry, path);
        defer allocator.free(url);

        var client = Client{ .allocator = allocator };
        defer client.deinit();

        const payload_fmt = "{{ \"schema\": \"{s}\", \"schemaType\": \"AVRO\" }}" ;

        const json_str = try jsonStringify(allocator, schema);
        defer allocator.free(json_str);

        const payload = try allocPrint(allocator, payload_fmt, .{ json_str });
        defer allocator.free(payload);

        var res_body = ArrayList(u8).init(allocator);
        defer res_body.deinit();
        const res = try client.fetch(.{
            .method = http.Method.POST,
            .payload = payload,
            .location = .{
                .url = url
            },
            .response_storage = .{
                .dynamic = &res_body
            }
        });

        if (res.status != .ok) {
            log.err("Unexpected HTTP response.\nStatus code: {d}\nResponse: {s}\n", .{ @intFromEnum(res.status), res_body.items });
        }

        const json_res = try json.parse(res_body.items, allocator);
        defer json_res.deinit(allocator);

        return @intCast(json_res.get("id").integer());
    }

    /// Fetch body from HTTP GET request. Caller owns memory.
    fn fetchBody(allocator: Allocator, base_url: []const u8, path: []const u8) ![]const u8 {
        const url = try parseUrl(allocator, base_url, path);
        defer allocator.free(url);

        var client = Client{ .allocator = allocator };
        defer client.deinit();

        var res_body = ArrayList(u8).init(allocator);
        defer res_body.deinit();
        const res = try client.fetch(.{
            .method = http.Method.GET,
            .location = .{
                .url = url
            },
            .response_storage = .{
                .dynamic = &res_body
            }
        });

        if (res.status != .ok) {
            log.err("Unexpected HTTP response.\nStatus code: {d}\nResponse: {s}\n", .{ res.status, res_body.items });
        }
        return try res_body.toOwnedSlice();
    }

    /// Converts a JSON schema to native Avro schema object
    fn jsonToAvro(schema: []const u8) Error!a.avro_schema_t {
        var avro_schema: a.avro_schema_t = undefined;
        const res = a.avro_schema_from_json_length(@constCast(schema.ptr), schema.len, &avro_schema);
        errdefer log.err("{s}\n", .{ a.avro_strerror() });

        if (res != 0) {
            return Error.AvroSchemaInitError;
        }

        return avro_schema;
    }
};

pub const AvroBytes = struct {
    val: []const u8,
    len: usize,

    /// Reads first value found from Avro bytes. Length is total bytes used for
    /// the value, including overhead.
    ///
    /// Doesn't work with complex types e.g. array that will specify length of
    /// items in array (instead of bytes used).
    pub fn read(bytes: []const u8) AvroBytes {
        const varIntLen = serialize.varIntLen(bytes);
        const valLen: usize = @intCast(serialize.decodeVarIntZigZag(bytes[0..varIntLen]));
        const val = bytes[varIntLen..varIntLen+valLen];
        return .{
            .val = val,
            .len = varIntLen + valLen
        };
    }
};

test "avro value: as json: all basic non-escaped values" {
    const allocator = testing.allocator;

    // Note: In Zig multiline strings ignore escapes like \xNN and \u{NNNNNN} or
    // JSON-string escapes like \u00NN. To use escaped characters use one liner
    // string.
    const json_str =
        \\{
        \\  "name": "John Doe",
        \\  "age": 42,
        \\  "active": true,
        \\  "null": null,
        \\  "union_null": null,
        \\  "bytes": "AAA",
        \\  "fixed": "AAA",
        \\  "enum": "Foo",
        \\  "fruits": [
        \\    "banana",
        \\    "apple"
        \\  ],
        \\  "map": {
        \\    "key1": "value1",
        \\    "key2": "value2"
        \\  },
        \\  "address": {
        \\    "city": "New York",
        \\    "zip": 65000
        \\  }
        \\}
    ;

    const schema =
        \\{
        \\  "type": "record",
        \\  "name": "AvroTypes",
        \\  "fields": [
        \\    { "name": "name", "type": "string" },
        \\    { "name": "age", "type": "int" },
        \\    { "name": "active", "type": "boolean" },
        \\    { "name": "null", "type": "null" },
        \\    { "name": "union_null", "type": ["int", "null"] },
        \\    { "name": "bytes", "type": "bytes" },
        \\    { "name": "fixed", "type": { "name": "simple", "type": "fixed", "size": 3 } },
        \\    { "name": "enum", "type": { "name": "foobar", "type": "enum", "symbols" : ["Foo", "Bar"] }},
        \\    { "name": "fruits", "type": { "type": "array", "items": "string" } },
        \\    { "name": "map", "type": { "type": "map", "values": "string"} },
        \\    {
        \\      "name": "address",
        \\      "type": {
        \\        "type" : "record",
        \\          "name" : "address.fields",
        \\          "fields" : [
        \\            { "name": "city", "type": "string" },
        \\            { "name": "zip", "type": "long" }
        \\          ]
        \\      }
        \\    }
        \\  ]
        \\}
    ;

    var avro_schema = try AvroSchema.fromJson(allocator, schema);
    defer avro_schema.deinit();

    const avro_val = try AvroValue.fromJson(allocator, json_str, &avro_schema);
    defer avro_val.deinit();

    const json_output = try avro_val.asJson();
    try testing.expectEqualStrings(json_output, json_str);
}

test "avro value: as json: deviating output float, double, union" {
    const allocator = testing.allocator;

    const json_str =
        \\{
        \\  "float": 3.1415,
        \\  "double": 3.14159265,
        \\  "union_string": "10"
        \\}
    ;
    const schema =
        \\{
        \\  "type": "record",
        \\  "name": "AvroTypes",
        \\  "fields": [
        \\    { "name": "float", "type": "float" },
        \\    { "name": "double", "type": "double" },
        \\    { "name": "union_string", "type": ["int", "string"] }
        \\  ]
        \\}
    ;

    var avro_schema = try AvroSchema.fromJson(allocator, schema);
    defer avro_schema.deinit();

    const avro_val = try AvroValue.fromJson(allocator, json_str, &avro_schema);
    defer avro_val.deinit();

    const json_output = try avro_val.asJson();
    const json_val = try json.parse(json_output, allocator);
    defer json_val.deinit(allocator);

    // Float and double loses some precision
    try testing.expectApproxEqRel(3.1415, json_val.get("float").float(), 1e-6);
    try testing.expectApproxEqRel(3.14159265, json_val.get("double").float(), 1e-12);

    // Unions, if not null, will be returned in format:
    // "union": {
    //   "type": "value"
    // }
    try testing.expectEqualStrings(json_val.get("union_string").object().get("string").string(), "10");
}

test "avro value: as json: bytes" {
    const allocator = testing.allocator;

    const json_str = "{ \"bytes\": \"\xE2\x9A\xA1\" }";
    const schema =
        \\{
        \\  "type": "record",
        \\  "name": "bytes",
        \\  "fields": [
        \\    { "name": "bytes", "type": "bytes" }
        \\  ]
        \\}
    ;

    var avro_schema = try AvroSchema.fromJson(allocator, schema);
    defer avro_schema.deinit();

    const avro_val = try AvroValue.fromJson(allocator, json_str, &avro_schema);
    defer avro_val.deinit();

    const json_output = try avro_val.asJson();
    try testing.expectEqualStrings(json_output, "{\n  \"bytes\": \"\\u00E2\\u009A\\u00A1\"\n}");
}

test "avro value: as json: bytes fixed size" {
    const allocator = testing.allocator;

    const json_str = "{ \"fixed\": \"\xE2\x9A\xA1\" }";
    const schema =
        \\{
        \\  "type": "record",
        \\  "name": "bytes",
        \\  "fields": [
        \\    { "name": "fixed", "type": { "name": "simple", "type": "fixed", "size": 3 } }
        \\  ]
        \\}
    ;

    var avro_schema = try AvroSchema.fromJson(allocator, schema);
    defer avro_schema.deinit();

    const avro_val = try AvroValue.fromJson(allocator, json_str, &avro_schema);
    defer avro_val.deinit();

    const json_output = try avro_val.asJson();
    try testing.expectEqualStrings(json_output, "{\n  \"fixed\": \"\\u00E2\\u009A\\u00A1\"\n}");
}

test "avro value: as json: decimal as bytes" {
    const allocator = testing.allocator;

    const json_str = "{ \"decimal\": \"3.141592\" }";
    const schema =
       \\{
       \\  "type": "record",
       \\  "name": "bytes",
        \\  "fields": [
       \\     { "name": "decimal", "type": { "type": "bytes", "logicalType": "decimal", "precision": 6, "scale": 2 } }
       \\  ]
       \\}
    ;

    var avro_schema = try AvroSchema.fromJson(allocator, schema);
    defer avro_schema.deinit();

    const avro_val = try AvroValue.fromJson(allocator, json_str, &avro_schema);
    defer avro_val.deinit();

    const json_output = try avro_val.asJson();
    const json_val = try json.parse(json_output, allocator);
    defer json_val.deinit(allocator);
    try testing.expectEqualStrings(json_val.get("decimal").string(), "3.141592");
}

test "avro value: as bytes confluent" {
    const allocator = testing.allocator;
    const json_str =
        \\{
        \\  "name": "John Doe",
        \\  "age": 42
        \\}
    ;
    const schema =
        \\{
        \\  "type": "record",
        \\  "name": "AvroTypes",
        \\  "fields": [
        \\    { "name": "name", "type": "string" },
        \\    { "name": "age", "type": "int" }
        \\  ]
        \\}
    ;

    const schema_id = 16843009; // 000000001 00000001 00000001 00000001
    var avro_schema = try AvroSchema.fromJsonWithId(allocator, schema, schema_id);
    defer avro_schema.deinit();

    const avro_val = try AvroValue.fromJson(allocator, json_str, &avro_schema);
    defer avro_val.deinit();

    const payload = try avro_val.asBytesConfluent();
    defer allocator.free(payload);

    try testing.expectEqual(payload[0], 0); // Magic byte 0
    try testing.expectEqual(payload[1], 1); // Schema id 1-4
    try testing.expectEqual(payload[2], 1); //
    try testing.expectEqual(payload[3], 1); //
    try testing.expectEqual(payload[4], 1); //
}

test "avro value: from json as bytes" {
    const allocator = testing.allocator;
    const bytes = [_]u8 { 6, 97, 98, 99, 6, 99, 100, 101, 253, 255, 255, 255,
    15, 0, 254, 255, 255, 255, 255, 255, 255, 255, 255, 1, 216, 15, 73, 64, 183,
    152, 131, 44, 251, 33, 9, 64, 2, 10, 85, 78, 73, 79, 78, 10, 66, 89, 84, 69,
    83, 70, 73, 88, 69, 68, 0, 8, 10, 97, 112, 112, 108, 101, 12, 98, 97, 110,
    97, 110, 97, 12, 111, 114, 97, 110, 103, 101, 8, 112, 101, 97, 114, 0, 6, 6,
    49, 49, 49, 6, 49, 50, 51, 6, 50, 50, 50, 6, 52, 53, 54, 6, 51, 51, 51, 6,
    55, 56, 57, 0, 6, 49, 50, 51, 6, 52, 53, 54 };

    const json_str =
        \\{
        \\  "name": "abc",
        \\  "last": "cde",
        \\  "age": -2147483647,
        \\  "active": false,
        \\  "long": 9223372036854775807,
        \\  "float": 3.141592,
        \\  "double": 3.141592357412345678910111213,
        \\  "null": null,
        \\  "union_null": "UNION",
        \\  "bytes": "BYTES",
        \\  "fixed": "FIXED",
        \\  "enum": "Foo",
        \\  "fruits": [
        \\    "apple",
        \\    "banana",
        \\    "orange",
        \\    "pear"
        \\  ],
        \\  "map": {
        \\    "111": "123",
        \\    "222": "456",
        \\    "333": "789"
        \\  },
        \\  "address": {
        \\    "city": "123",
        \\    "zip": "456"
        \\  }
        \\}
    ;

    const schema =
        \\{
        \\  "type": "record",
        \\  "name": "AvroTypes",
        \\  "fields": [
        \\    { "name": "name", "type": "string" },
        \\    { "name": "last", "type": "string" },
        \\    { "name": "age", "type": "int" },
        \\    { "name": "active", "type": "boolean" },
        \\    { "name": "long", "type": "long" },
        \\    { "name": "float", "type": "float" },
        \\    { "name": "double", "type": "double" },
        \\    { "name": "null", "type": "null" },
        \\    { "name": "union_null", "type": ["null", "string"] },
        \\    { "name": "bytes", "type": "bytes" },
        \\    { "name": "fixed", "type": { "name": "simple", "type": "fixed", "size": 5 } },
        \\    { "name": "enum", "type": { "name": "foobar", "type": "enum", "symbols" : ["Foo", "Bar"] } },
        \\    { "name": "fruits", "type": { "type": "array", "items": "string" } },
        \\    { "name": "map", "type": { "type": "map", "values": "string"} },
        \\    {
        \\      "name": "address",
        \\      "type": {
        \\        "type" : "record",
        \\          "name" : "address.fields",
        \\          "fields" : [
        \\            { "name": "city", "type": "string" },
        \\            { "name": "zip", "type": "string" }
        \\          ]
        \\      }
        \\    }
        \\  ]
        \\}
    ;

    var avro_schema = try AvroSchema.fromJson(allocator, schema);
    defer avro_schema.deinit();

    const avro_val = try AvroValue.fromJson(allocator, json_str, &avro_schema);
    defer avro_val.deinit();

    const val = try avro_val.asBytes();
    defer allocator.free(val);

    try testing.expectEqualSlices(u8, &bytes, val);
}

test "avro value: from bytes as bytes" {
    const allocator = testing.allocator;
    const bytes = [_]u8 { 6, 97, 98, 99, 6, 99, 100, 101, 253, 255, 255, 255,
    15, 0, 254, 255, 255, 255, 255, 255, 255, 255, 255, 1, 216, 15, 73, 64, 183,
    152, 131, 44, 251, 33, 9, 64, 2, 10, 85, 78, 73, 79, 78, 10, 66, 89, 84, 69,
    83, 70, 73, 88, 69, 68, 0, 8, 10, 97, 112, 112, 108, 101, 12, 98, 97, 110,
    97, 110, 97, 12, 111, 114, 97, 110, 103, 101, 8, 112, 101, 97, 114, 0, 6, 6,
    49, 49, 49, 6, 49, 50, 51, 6, 50, 50, 50, 6, 52, 53, 54, 6, 51, 51, 51, 6,
    55, 56, 57, 0, 6, 49, 50, 51, 6, 52, 53, 54 };

    // Bytes value:
    // {
    //   "name": "abc",
    //   "last": "cde",
    //   "age": -2147483647,
    //   "active": false,
    //   "long": 9223372036854775807,
    //   "float": 3.141592,
    //   "double": 3.141592357412345678910111213,
    //   "null": null,
    //   "union_null": "UNION",
    //   "bytes": "BYTES",
    //   "fixed": "FIXED",
    //   "enum": "Foo",
    //   "fruits": [
    //     "apple",
    //     "banana",
    //     "orange",
    //     "pear"
    //   ],
    //   "map": {
    //     "111": "123",
    //     "222": "456",
    //     "333": "789"
    //   },
    //   "address": {
    //     "city": "123",
    //     "zip": "456"
    //   }
    // }

    const schema =
        \\{
        \\  "type": "record",
        \\  "name": "AvroTypes",
        \\  "fields": [
        \\    { "name": "name", "type": "string" },
        \\    { "name": "last", "type": "string" },
        \\    { "name": "age", "type": "int" },
        \\    { "name": "active", "type": "boolean" },
        \\    { "name": "long", "type": "long" },
        \\    { "name": "float", "type": "float" },
        \\    { "name": "double", "type": "double" },
        \\    { "name": "null", "type": "null" },
        \\    { "name": "union_null", "type": ["null", "string"] },
        \\    { "name": "bytes", "type": "bytes" },
        \\    { "name": "fixed", "type": { "name": "simple", "type": "fixed", "size": 5 } },
        \\    { "name": "enum", "type": { "name": "foobar", "type": "enum", "symbols" : ["Foo", "Bar"] } },
        \\    { "name": "fruits", "type": { "type": "array", "items": "string" } },
        \\    { "name": "map", "type": { "type": "map", "values": "string"} },
        \\    {
        \\      "name": "address",
        \\      "type": {
        \\        "type" : "record",
        \\          "name" : "address.fields",
        \\          "fields" : [
        \\            { "name": "city", "type": "string" },
        \\            { "name": "zip", "type": "string" }
        \\          ]
        \\      }
        \\    }
        \\  ]
        \\}
    ;

    var avro_schema = try AvroSchema.fromJson(allocator, schema);
    defer avro_schema.deinit();

    const avro_val = try AvroValue.fromBytes(allocator, &bytes, &avro_schema);
    defer avro_val.deinit();

    const val = try avro_val.asBytes();
    defer allocator.free(val);

    try testing.expectEqualSlices(u8, &bytes, val);
}

test "avro bytes: read bytes" {
    const bytes = [_]u8 { 6, 97, 98, 99 }; // "abc"
    const bytes_ext = [_]u8 { 6, 97, 98, 99, 6, 97, 98, 99 }; // "abc", "abc"

    const avro_bytes = AvroBytes.read(&bytes);
    const avro_bytes_ext = AvroBytes.read(&bytes_ext);

    try testing.expectEqual(4, avro_bytes.len);
    try testing.expectEqualSlices(u8, &.{ 97, 98, 99 }, avro_bytes.val);
    try testing.expectEqual(4, avro_bytes_ext.len);
    try testing.expectEqualSlices(u8, &.{ 97, 98, 99 }, avro_bytes_ext.val);
}

test {
    std.testing.refAllDecls(@This());
}
