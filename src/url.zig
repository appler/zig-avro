const std = @import("std");
const mem = std.mem;
const eql = std.mem.eql;
const testing = std.testing;

const Allocator = mem.Allocator;

/// Simple URL parse to handle extra slash (/) in url or path.
/// Caller owns memory.
pub fn parseUrl(allocator: Allocator, base_url: []const u8, path: []const u8) ![]const u8 {
    const base_url_end: usize =
        if (eql(u8, base_url[base_url.len - 1..base_url.len], "/")) base_url.len - 1 else base_url.len;
    const path_start: usize = if (eql(u8, path[0..1], "/")) 1 else 0;

    const url = try std.fmt.allocPrint(allocator, "{s}/{s}", .{ base_url[0..base_url_end], path[path_start..] });

    return url;
}

test "avro schema: parse url" {
    const allocator = testing.allocator;
    const url1 = try parseUrl(allocator, "http://127.0.0.1:8081/", "/schemas/ids/1");
    const url2 = try parseUrl(allocator, "http://127.0.0.1:8081/", "schemas/ids/1");
    const url3 = try parseUrl(allocator, "http://127.0.0.1:8081", "/schemas/ids/1");
    const url4 = try parseUrl(allocator, "http://127.0.0.1:8081", "schemas/ids/1");
    defer allocator.free(url1);
    defer allocator.free(url2);
    defer allocator.free(url3);
    defer allocator.free(url4);

    try testing.expectEqualStrings("http://127.0.0.1:8081/schemas/ids/1", url1);
    try testing.expectEqualStrings("http://127.0.0.1:8081/schemas/ids/1", url2);
    try testing.expectEqualStrings("http://127.0.0.1:8081/schemas/ids/1", url3);
    try testing.expectEqualStrings("http://127.0.0.1:8081/schemas/ids/1", url4);
}
