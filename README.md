# Zig Avro

This is client library based on [avro](https://avro.apache.org/docs/current/api/c/index.html)

## Usage

Create Avro schema with registry and create Avro value:
```zig
test "avro schema and value" {
    const allocator = testing.allocator;
    const json_str =
        \\{
        \\  "name": "John Doe",
        \\  "age": 42,
        \\  "active": true,
        \\  "null": null,
        \\  "union_null": null,
        \\  "bytes": "AAA",
        \\  "fixed": "AAA",
        \\  "enum": "Foo",
        \\  "fruits": [
        \\    "banana",
        \\    "apple"
        \\  ],
        \\  "map": {
        \\    "key1": "value1",
        \\    "key2": "value2"
        \\  },
        \\  "address": {
        \\    "city": "New York",
        \\    "zip": 65000
        \\  }
        \\}
    ;

    const schema =
        \\{
        \\  "type": "record",
        \\  "name": "AvroTypes",
        \\  "fields": [
        \\    { "name": "name", "type": "string" },
        \\    { "name": "age", "type": "int" },
        \\    { "name": "active", "type": "boolean" },
        \\    { "name": "null", "type": "null" },
        \\    { "name": "union_null", "type": ["int", "null"] },
        \\    { "name": "bytes", "type": "bytes" },
        \\    { "name": "fixed", "type": { "name": "simple", "type": "fixed", "size": 3 } },
        \\    { "name": "enum", "type": { "name": "foobar", "type": "enum", "symbols" : ["Foo", "Bar"] }},
        \\    { "name": "fruits", "type": { "type": "array", "items": "string" } },
        \\    { "name": "map", "type": { "type": "map", "values": "string"} },
        \\    {
        \\      "name": "address",
        \\      "type": {
        \\        "type" : "record",
        \\          "name" : "address.fields",
        \\          "fields" : [
        \\            { "name": "city", "type": "string" },
        \\            { "name": "zip", "type": "long" }
        \\          ]
        \\      }
        \\    }
        \\  ]
        \\}
    ;

    var avro_schema = try AvroSchema.fromJsonWithRegistry(allocator, schema, "http://127.0.0.1:8081");
    defer avro_schema.deinit();

    const avro_val = try AvroValue.fromJson(allocator, json_str, &avro_schema);
    defer avro_val.deinit();

    const json_output = try avro_val.asJson();
    try testing.expectEqualStrings(json_output, json_str);
}
```

To get Avro value as Confluent format (with magic byte and schema id).
```zig
const avro_val = try AvroValue.fromJson(allocator, json_str, &avro_schema);
defer avro_val.deinit();

const avro_message = try avro_val.asBytesConfluent();
```

## Installation

Add dependency to `build.zig.zon`:
```sh
zig fetch --save https://codeberg.org/appler/zig-avro/archive/v0.0.2.tar.gz
```

Update `build.zig` with import for module:
```zig
const avro_mod = b.dependency("zig-avro", .{
    .target = target,
    .optimize = optimize
}).module("avro");

your_compilation.root_module.addImport("avro", avro_mod);
```

Import `avro` in your project:
```zig
const kafka = @import("avro");
```

## Dependencies

Requires [jansson](http://www.digip.org/jansson/) that is required by [avro](https://codeberg.org/appler/avro).
