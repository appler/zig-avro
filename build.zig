const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const avro_mod = b.addModule("avro", .{
        .root_source_file = b.path("src/avro.zig"),
        .target = target,
        .optimize = optimize
    });

    const lib = b.addStaticLibrary(.{
        .name = "zig-avro",
        .root_source_file = b.path("src/avro.zig"),
        .target = target,
        .optimize = optimize,
    });

    const lib_unit_tests = b.addTest(.{
        .root_source_file = b.path("src/avro.zig"),
        .target = target,
        .optimize = optimize,
    });

    const run_lib_unit_tests = b.addRunArtifact(lib_unit_tests);
    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_lib_unit_tests.step);

    const json_dep = b.dependency("zig-json", .{ .target = target, .optimize = optimize });
    lib.root_module.addImport("json", json_dep.module("zig-json"));
    lib_unit_tests.root_module.addImport("json", json_dep.module("zig-json"));
    avro_mod.addImport("json", json_dep.module("zig-json"));

    const avro_dep = b.dependency("avro", .{ .target = target, .optimize = optimize });
    lib.linkLibrary(avro_dep.artifact("avro"));
    lib_unit_tests.linkLibrary(avro_dep.artifact("avro"));
    avro_mod.linkLibrary(avro_dep.artifact("avro"));

    b.installArtifact(lib);
}
